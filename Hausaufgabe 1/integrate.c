#include <stdio.h>
#include <stdlib.h>

int main() {
   // Hier kommen die ganzen Befehle hin
   double left = 0.0;
   double right = 2.0;
   int N = 1000;
   //double sum = 0;

   double delta_y = (right - left)/N;

   double sum = 0.0;
   for(int k = 0; k < N; ++k){
       double y = left + k * delta_y;
       double f = y*y*y - y/2;
       double A = f *delta_y;
       sum += A;
       } 
    printf("Integralwert analytisch: 3\n");
    printf("Integralwert numerisch: %f\n", sum); 

    
   return EXIT_SUCCESS;
}