#include <stdio.h>
#include <stdlib.h>


//Definiere Funktion p1, das ist eine beliebige Funktion, die double einliest und zurückgibt
double p1(double y)
{
return y*y*y - y/2;
}

//Definiere mit integrate eine Integrationsfunktion, die neben verschiedenen Parametern eine Funktion mittels integrand als Argument hat, welche integriert werden soll

double integrate(double left, double right, int N, double integrand(double))

{
    double sum = 0;
    double delta_y = (right - left)/N;

    for (int k = 0; k < N; ++k)  {
        double y = left + k * delta_y;
        double f = integrand(y);
        double A = f*delta_y;
        sum += A;

    }

    return sum;
}


int main(void) {
  double left = 0.;
  double right = 2.;
  int N = 100;

  double result = integrate(left, right, N, p1);
    printf("Integralwert analytisch: 3\n");
    printf("Integralwert numerisch: %f\n", result); 

    
   return EXIT_SUCCESS;
}