#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double x = 1.0;
double integral = 1.1752011936; // nutze hier einfach den Funktionswert von sinh() an der Stelle 1
double cosh_midpoint(double delta_x) {
    double sum = 0.0;
    int N = x/delta_x;
    for(int k = 0; k < N; ++k){
       double A = delta_x* (cosh(delta_x * k + delta_x/2));
       sum += A;
       } 
    return sum;
    }

 double cosh_simpson(double delta_x) {
      double summe = 0.0;
    int M = x/delta_x;
    for(int k = 0; k < M; ++k){
        double B = delta_x/6 * (cosh(delta_x * k) +  4*cosh(delta_x * k + delta_x/2) + cosh(delta_x*(k+1)));
        summe += B;
        }

    return summe;
    } 


int main(void) {
    FILE *data1;
    data1 = fopen("daten1.csv", "w");
    for(double x = 1; x<=200; ++x){
        double count = 2/x;
        double result = cosh_midpoint(count);
        double value = integral - result;
        fprintf(data1, "%f, %f\n", count, value);

    }
    fclose(data1);
  
    FILE *data2;
    data2 = fopen("daten2.csv", "w");
    for(double x = 1; x<=200; ++x){
        double count = 2/x;
        double result = cosh_simpson(count);
        double value = integral - result;
        fprintf(data2, "%f, %f\n", count, value);

    }
    fclose(data2);
    }
