#include <stdio.h>
#include <stdlib.h>
#include <math.h>


double erf_midpoint(double x, double delta_x) {
    double sum = 0.0;
    int N = x/delta_x;
    for(int k = 0; k < N; ++k){
       double A = delta_x*2*(1/(sqrt(M_PI)))*exp(-(delta_x * k +delta_x/2)*(delta_x * k +delta_x/2)); //hier die richtige Funktion und Formel einfügen, dann die Simpson-Formel 
       sum += A;
       } 
    return sum;
    }

double erf_simpson(double x, double delta_x) {
    double summe = 0.0;
    int M = x/delta_x;
    for(int k = 0; k < M; ++k){
        double B = (2/(6*sqrt(M_PI)))* delta_x*( exp(-(delta_x * k)*(delta_x * k)) + 4*exp(-(delta_x * k +delta_x/2)*(delta_x * k +delta_x/2)) +   exp(-(delta_x * (k+1))*(delta_x * (k+1))));
        summe += B;
        }

    return summe;
    }


int main() {
    

    double result1 = erf_midpoint(3, 0.1);
    double result2 = erf_simpson(3, 0.0001);
    printf(" Midpoint: %f\n", result1);
     printf("Simpson: %f", result2);
        
    
    }


