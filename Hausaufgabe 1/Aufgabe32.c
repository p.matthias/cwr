#include <stdio.h>
#include <stdlib.h>
#include <math.h>




double erf_simpson(double x, double delta_x) {
    double summe = 0.0;
    double M = x/delta_x;
    if (M > 0)
    {
    for(int k = 0; k < M; ++k){
        double B = (2/(6*sqrt(M_PI)))* delta_x*( exp(-(delta_x * k)*(delta_x * k)) + 4*exp(-(delta_x * k +delta_x/2)*(delta_x * k +delta_x/2)) +   exp(-(delta_x * (k+1))*(delta_x * (k+1))));
        summe += B;
        }

    return summe;
    }
    if (M < 0){
        for(int k = 0; k < abs(M); ++k){
        double B = -(2/(6*sqrt(M_PI)))* delta_x*( exp(-(delta_x * k)*(delta_x * k)) + 4*exp(-(delta_x * k +delta_x/2)*(delta_x * k +delta_x/2)) +   exp(-(delta_x * (k+1))*(delta_x * (k+1))));
        summe += B;
        }

    return summe;
    }
    }




int main(void) {
    FILE *datei;
    datei = fopen("data.csv", "w");
    for(double x = 0; x<=100; ++x){
    double count = -2 + x*4/100;
    double num = erf_simpson(count, 1e-4);
    fprintf(datei, "%f, %f\n", count, num);

    }
   fclose(datei);

    }

